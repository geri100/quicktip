# QuickTip

## Run
**mvn spring-boot:run -Dspring-boot.run.arguments="X Y"**
* X = strategy type
* Y = shows the number of tickets

Strategy type could be 1,2,3

### First algorithm (1)
It will be generate Z numbers.

Z = come from XML

### Second algorithm (2)
It will be generated ZxR numbers.

* Z,R = come from XML
    * Z number of numbers
    * R number of fields

* maximum number is fixed at 90

### Third algorithm (3)
It will be generated ZxR numbers.

* Z,R = come from XML
    * Z number of numbers
    * R number of fields