package com.lottery.scan.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Objects;
import java.util.StringJoiner;

@JacksonXmlRootElement(localName = "lottery")
public class SimpleLotteryXML {

    @JacksonXmlProperty(localName = "maxNumber")
    private int maxNumber;
    @JacksonXmlProperty(localName = "pieceOfNumber")
    private int pieceOfNumber;

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public int getPieceOfNumber() {
        return pieceOfNumber;
    }

    public void setPieceOfNumber(int pieceOfNumber) {
        this.pieceOfNumber = pieceOfNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleLotteryXML that = (SimpleLotteryXML) o;
        return maxNumber == that.maxNumber &&
                pieceOfNumber == that.pieceOfNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxNumber, pieceOfNumber);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SimpleLotteryXML.class.getSimpleName() + "[", "]")
                .add("maxNumber=" + maxNumber)
                .add("pieceOfNumber=" + pieceOfNumber)
                .toString();
    }
}
