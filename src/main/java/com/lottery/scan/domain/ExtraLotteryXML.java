package com.lottery.scan.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "lottery")
public class ExtraLotteryXML extends SimpleLotteryXML {

    @JacksonXmlProperty(localName = "fieldsNumber")
    private int fieldsNumber;

    public int getFieldsNumber() {
        return fieldsNumber;
    }

    public void setFieldsNumber(int fieldsNumber) {
        this.fieldsNumber = fieldsNumber;
    }

}
