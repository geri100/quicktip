package com.lottery.scan.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.reader.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class XMLScanner<E, S> implements Scanner<S> {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLScanner.class);

    private ObjectMapper objectMapper;
    private Class<E> classType;
    private FileReader fileReader;

    public XMLScanner(ObjectMapper objectMapper, Class<E> classType, FileReader fileReader) {
        this.objectMapper = objectMapper;
        this.classType = classType;
        this.fileReader = fileReader;
    }

    /**
     * Unmarshal from xml to object
     *
     * @return E type of object.
     */
    protected E scanFromXML() {
        try {
            var fileContent = fileReader.read(getFileUrl());
            return objectMapper.readValue(fileContent, classType);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    protected abstract String getFileUrl();

}
