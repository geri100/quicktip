package com.lottery.scan.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.converter.Converter;
import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.reader.FileReader;
import com.lottery.scan.domain.SimpleLotteryXML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SimpleXMLScanner extends XMLScanner<SimpleLotteryXML, SimpleLottery> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleXMLScanner.class);

    @Value("${xml.simple}")
    private String fileUrl;
    private Converter<SimpleLotteryXML, SimpleLottery> converter;

    @Autowired
    public SimpleXMLScanner(ObjectMapper objectMapper, Converter<SimpleLotteryXML, SimpleLottery> converter,
                            FileReader fileReader) {
        super(objectMapper, SimpleLotteryXML.class, fileReader);
        this.converter = converter;
    }

    @Override
    public SimpleLottery scan() {
        var simpleLotteryXML = scanFromXML();
        LOGGER.info("The {} has been transformed to {}", getFileUrl(), simpleLotteryXML);
        return converter.convert(simpleLotteryXML, SimpleLottery.class);
    }

    @Override
    public StrategyType getStrategyType() {
        return StrategyType.SIMPLE;
    }

    @Override
    protected String getFileUrl() {
        return fileUrl;
    }
}
