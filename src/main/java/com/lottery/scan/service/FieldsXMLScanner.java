package com.lottery.scan.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.converter.Converter;
import com.lottery.domain.ExtraLottery;
import com.lottery.domain.StrategyType;
import com.lottery.reader.FileReader;
import com.lottery.scan.domain.ExtraLotteryXML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FieldsXMLScanner extends ExtraXMLScanner {

    @Value("${xml.fields}")
    private String fileUrl;
    private int maxNumber;

    @Autowired
    public FieldsXMLScanner(ObjectMapper objectMapper, Converter<ExtraLotteryXML, ExtraLottery> converter,
                            FileReader fileReader, @Value("${default.maxNumber}") int defaultMaxNumber) {
        super(objectMapper, converter, fileReader);
        this.maxNumber = defaultMaxNumber;
    }

    @Override
    public ExtraLottery scan() {
        var extraLottery = super.scan();
        extraLottery.setMaxNumber(maxNumber);
        return extraLottery;
    }

    @Override
    public StrategyType getStrategyType() {
        return StrategyType.FIELDS;
    }

    @Override
    protected String getFileUrl() {
        return fileUrl;
    }
}
