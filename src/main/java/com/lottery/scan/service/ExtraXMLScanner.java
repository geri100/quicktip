package com.lottery.scan.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.converter.Converter;
import com.lottery.domain.ExtraLottery;
import com.lottery.domain.StrategyType;
import com.lottery.reader.FileReader;
import com.lottery.scan.domain.ExtraLotteryXML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExtraXMLScanner extends XMLScanner<ExtraLotteryXML, ExtraLottery> {

    @Value("${xml.extra}")
    private String fileUrl;
    private Converter<ExtraLotteryXML, ExtraLottery> converter;

    @Autowired
    public ExtraXMLScanner(ObjectMapper objectMapper, Converter<ExtraLotteryXML, ExtraLottery> converter,
                           FileReader fileReader) {
        super(objectMapper, ExtraLotteryXML.class, fileReader);
        this.converter = converter;
    }

    @Override
    public ExtraLottery scan() {
        var ExtraLotteryXML = scanFromXML();
        return converter.convert(ExtraLotteryXML, ExtraLottery.class);
    }

    @Override
    public StrategyType getStrategyType() {
        return StrategyType.EXTRA;
    }

    @Override
    protected String getFileUrl() {
        return fileUrl;
    }
}
