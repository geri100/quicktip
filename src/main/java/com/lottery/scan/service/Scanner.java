package com.lottery.scan.service;

import com.lottery.domain.StrategyType;

public interface Scanner<S> {

    /**
     * Scan from file to objects.
     *
     * @return {@link S}.
     */
    S scan();

    StrategyType getStrategyType();
}
