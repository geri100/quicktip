package com.lottery.scan;

import com.lottery.domain.StrategyType;
import com.lottery.exception.IncorrectStrategyException;
import com.lottery.scan.service.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ScannerFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScannerFactory.class);

    private Map<StrategyType, Scanner> scanners;

    @Autowired
    public ScannerFactory(Set<Scanner> scanners) {
        this.scanners = new HashMap<>();
        createScanners(scanners);
    }

    public <E> Scanner<E> findScanner(StrategyType strategyType) {
        var scanner = (Scanner<E>) scanners.get(strategyType);
        if (scanner != null) {
            LOGGER.info("The factory found {} scanner", scanner.getClass());
            return scanner;
        } else {
            LOGGER.error("Incorrect strategy type: {}", strategyType);
            throw new IncorrectStrategyException(strategyType);
        }
    }

    private void createScanners(Set<Scanner> scanners) {
        scanners.forEach(scanner -> this.scanners.put(scanner.getStrategyType(), scanner));
    }
}
