package com.lottery.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class FileReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileReader.class);

    public String read(String fileUrl) {
        try {
            return new String(Files.readAllBytes(Paths.get(fileUrl)));
        } catch (IOException e) {
            LOGGER.error("File read error: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
