package com.lottery;

import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Ticket;
import com.lottery.generate.GeneratorFactory;
import com.lottery.print.Printout;
import com.lottery.scan.ScannerFactory;
import com.lottery.scan.service.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuickTip {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuickTip.class);

    @Autowired
    private ScannerFactory scannerFactory;
    @Autowired
    private GeneratorFactory generatorFactory;
    @Autowired
    private Printout<List<Ticket>> printout;

    public void createTip(StrategyType strategyType, int ticketNumber) {
        LOGGER.info("CreateTip has been involved with {} strategy", strategyType);
        SimpleLottery simpleLottery = getLottery(strategyType);
        var tickets = getTickets(strategyType, ticketNumber, simpleLottery);
        printout.print(tickets);
    }

    private SimpleLottery getLottery(StrategyType strategyType) {
        Scanner<SimpleLottery> scanner = scannerFactory.findScanner(strategyType);
        return scanner.scan();
    }

    private List<Ticket> getTickets(StrategyType strategyType, int ticketNumber, SimpleLottery simpleLottery) {
        var generator = generatorFactory.findGenerator(strategyType);
        List<Ticket> tickets = new ArrayList<>();
        for (int i = 0; i < ticketNumber; i++) {
            tickets.add(generator.generate(simpleLottery));
        }
        return tickets;
    }
}
