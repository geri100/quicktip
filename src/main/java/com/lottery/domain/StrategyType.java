package com.lottery.domain;

public enum StrategyType {
    SIMPLE,
    FIELDS,
    EXTRA
}
