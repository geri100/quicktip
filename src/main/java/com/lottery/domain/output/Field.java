package com.lottery.domain.output;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Field {

    private List<Integer> numbers;

    public Field(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Field)) return false;
        Field field = (Field) o;
        return numbers.equals(field.numbers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numbers);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Field.class.getSimpleName() + "[", "]")
                .add("numbers=" + numbers)
                .toString();
    }

    public String printNumbers() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int number : numbers) stringBuilder.append(number + " ");
        return stringBuilder.toString();
    }
}
