package com.lottery.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class SimpleLottery {

    private int maxNumber;
    private int pieceOfNumber;

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public int getPieceOfNumber() {
        return pieceOfNumber;
    }

    public void setPieceOfNumber(int pieceOfNumber) {
        this.pieceOfNumber = pieceOfNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleLottery that = (SimpleLottery) o;
        return maxNumber == that.maxNumber &&
                pieceOfNumber == that.pieceOfNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxNumber, pieceOfNumber);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SimpleLottery.class.getSimpleName() + "[", "]")
                .add("maxNumber=" + maxNumber)
                .add("pieceOfNumber=" + pieceOfNumber)
                .toString();
    }
}

