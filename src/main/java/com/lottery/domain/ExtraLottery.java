package com.lottery.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class ExtraLottery extends SimpleLottery {

    private int fieldsNumber;

    public ExtraLottery() {
        super();
    }

    public int getFieldsNumber() {
        return fieldsNumber;
    }

    public void setFieldsNumber(int fieldsNumber) {
        this.fieldsNumber = fieldsNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ExtraLottery that = (ExtraLottery) o;
        return fieldsNumber == that.fieldsNumber &&
                getMaxNumber() == that.getMaxNumber() &&
                getPieceOfNumber() == that.getPieceOfNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), fieldsNumber);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ExtraLottery.class.getSimpleName() + "[", "]")
                .add("fieldsNumber=" + fieldsNumber)
                .add("maxNumber=" + getMaxNumber())
                .add("pieceOfNumber=" + getPieceOfNumber())
                .toString();
    }
}

