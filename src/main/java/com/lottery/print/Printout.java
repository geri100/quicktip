package com.lottery.print;

public interface Printout<E> {

    void print(E e);
}
