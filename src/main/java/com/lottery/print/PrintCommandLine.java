package com.lottery.print;

import com.lottery.domain.output.Field;
import com.lottery.domain.output.Ticket;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrintCommandLine implements Printout<List<Ticket>> {

    @Override
    public void print(List<Ticket> tickets) {
        System.out.println("-------Numbers-------");
        for (Ticket ticket : tickets) {
            for (Field field : ticket.getFields()) {
                System.out.print(field.printNumbers());
                System.out.println("");
            }
            System.out.println("--------------");
        }
        System.out.println();
    }
}
