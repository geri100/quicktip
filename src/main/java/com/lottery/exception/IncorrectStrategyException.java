package com.lottery.exception;

import com.lottery.domain.StrategyType;

import static java.text.MessageFormat.format;

public class IncorrectStrategyException extends RuntimeException {

    public IncorrectStrategyException(StrategyType strategyType) {
        super(format("Incorrect strategy type: {0}", strategyType));
    }
}
