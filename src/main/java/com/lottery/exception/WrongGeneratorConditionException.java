package com.lottery.exception;

public class WrongGeneratorConditionException extends RuntimeException {

    public WrongGeneratorConditionException(String message) {
        super(message);
    }
}
