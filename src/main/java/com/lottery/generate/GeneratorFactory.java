package com.lottery.generate;

import com.lottery.domain.StrategyType;
import com.lottery.exception.IncorrectStrategyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class GeneratorFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneratorFactory.class);

    private Map<StrategyType, Generator> generators;

    @Autowired
    public GeneratorFactory(Set<Generator> generators) {
        this.generators = new HashMap<>();
        createGenerator(generators);
    }

    /**
     * It will search for the associated generator.
     *
     * @param strategyType type of generator.
     * @param <E>          type generator.
     * @return E type of generator.
     */
    public <E> Generator<E> findGenerator(StrategyType strategyType) {
        var generator = (Generator<E>) generators.get(strategyType);
        if (generator != null) {
            LOGGER.info("The factory found {} generator", generator.getClass());
            return generator;
        } else {
            LOGGER.error("Incorrect strategy type: {}", strategyType);
            throw new IncorrectStrategyException(strategyType);
        }
    }

    private void createGenerator(Set<Generator> generators) {
        generators.forEach(generator -> this.generators.put(generator.getStrategyType(), generator));
    }

}
