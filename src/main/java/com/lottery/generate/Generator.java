package com.lottery.generate;

import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Ticket;

public interface Generator<E> {

    Ticket generate(E e);

    StrategyType getStrategyType();
}
