package com.lottery.generate;

import com.lottery.domain.ExtraLottery;
import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Field;
import com.lottery.domain.output.Ticket;
import com.lottery.exception.WrongGeneratorConditionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.text.MessageFormat.format;

@Service
public class ExtraGenerator implements Generator<ExtraLottery> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldsGenerator.class);

    @Autowired
    private SimpleGenerator simpleGenerator;

    /**
     * Generate X * Y piece of numbers and stores in the map with key.
     * Key start from 0 to Y.
     * X piece of number / list.
     *
     * @param extraLottery from  which we extract the maximum number and the number of pieces.
     * @return a {@link Map} which contains one list. The list key is zero.
     */
    @Override
    public Ticket generate(ExtraLottery extraLottery) {
        checkConditions(extraLottery);
        var fields = new ArrayList<Field>();
        for (int i = 0; i < extraLottery.getFieldsNumber(); i++) {
            fields.addAll(simpleGenerator.generate(extraLottery).getFields());
        }
        return new Ticket(fields);
    }

    @Override
    public StrategyType getStrategyType() {
        return StrategyType.EXTRA;
    }

    private void checkConditions(ExtraLottery extraLottery) {
        if (extraLottery.getFieldsNumber() < 1) {
            String message = format("Wrong generator condition: fields number({0}) should be greater than 1",
                    extraLottery.getFieldsNumber());
            LOGGER.error(message);
            throw new WrongGeneratorConditionException(message);
        }
    }

}
