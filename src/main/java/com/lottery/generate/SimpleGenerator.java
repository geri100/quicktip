package com.lottery.generate;

import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Field;
import com.lottery.domain.output.Ticket;
import com.lottery.exception.WrongGeneratorConditionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.text.MessageFormat.format;

@Service
public class SimpleGenerator implements Generator<SimpleLottery> {

    public static final int MIN_NUMBER = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleGenerator.class);

    @Autowired
    private NumberGenerator numberGenerator;

    /**
     * Generate X piece of numbers.
     *
     * @param simpleLottery from  which we extract the maximum number and the number of pieces.
     * @return a {@link List} which contains one {@link Ticket}.
     */
    @Override
    public Ticket generate(SimpleLottery simpleLottery) {
        checkConditions(simpleLottery);
        var numbers = new TreeSet<Integer>();
        while (numbers.size() < simpleLottery.getPieceOfNumber()) {
            numbers.add(numberGenerator.generateRandomNumber(MIN_NUMBER, simpleLottery.getMaxNumber()));
        }
        return createTickets(numbers);
    }

    @Override
    public StrategyType getStrategyType() {
        return StrategyType.SIMPLE;
    }

    private void checkConditions(SimpleLottery simpleLottery) {
        if (simpleLottery.getMaxNumber() - MIN_NUMBER + 1 < simpleLottery.getPieceOfNumber()) {
            String message = format("Wrong generator condition: max number({0}) should be greater than piece of number({1})",
                    simpleLottery.getMaxNumber(), simpleLottery.getPieceOfNumber());
            LOGGER.error(message);
            throw new WrongGeneratorConditionException(message);
        }
    }

    private Ticket createTickets(Set<Integer> numbers) {
        var field = new Field(new ArrayList<>(numbers));
        return new Ticket(List.of(field));
    }
}
