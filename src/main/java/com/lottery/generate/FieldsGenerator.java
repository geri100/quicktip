package com.lottery.generate;

import com.lottery.domain.StrategyType;
import org.springframework.stereotype.Service;

@Service
public class FieldsGenerator extends ExtraGenerator {

    @Override
    public StrategyType getStrategyType() {
        return StrategyType.FIELDS;
    }
}
