package com.lottery.generate;

import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class NumberGenerator {

    /**
     * Generate a random number.
     *
     * @param min beginning of interval.
     * @param max end of interval.
     * @return random number.
     */
    public int generateRandomNumber(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
