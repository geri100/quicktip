package com.lottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuickTipApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuickTipApplication.class, args);
    }
}
