package com.lottery.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Converter<S, D> {

    @Autowired
    private ModelMapper modelMapper;

    public D convert(S source, Class<D> classType) {
        return modelMapper.map(source, classType);
    }

}
