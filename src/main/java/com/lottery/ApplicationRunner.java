package com.lottery;

import com.lottery.domain.StrategyType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import static java.text.MessageFormat.format;

@Service
public class ApplicationRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationRunner.class);

    @Autowired
    private QuickTip quickTip;
    @Autowired
    private Environment env;

    @Override
    public void run(String... args) {
        checkArgs(args);
        LOGGER.info(format("Application started with this strategy : {0}", args));
        var strategyType = getStrategyType(args[0]);
        int ticketNumber = Integer.parseInt(args[1]);
        quickTip.createTip(strategyType, ticketNumber);
    }

    private void checkArgs(String[] args) {
        if (args.length <= 0) {
            throw new IllegalArgumentException("You have to add a strategy.");
        } else if (args.length > 2) {
            throw new IllegalArgumentException("You have to add only one strategy and tickets number.");
        } else if (args.length == 1) {
            throw new IllegalArgumentException("You have to add a strategy and tickets number.");
        }
    }

    private StrategyType getStrategyType(String strategyNumber) {
        try {
            var strategyName = env.getProperty("strategy." + strategyNumber);
            return StrategyType.valueOf(strategyName);
        } catch (Exception e) {
            LOGGER.error("Incorrect strategy number: {}", strategyNumber, e);
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
