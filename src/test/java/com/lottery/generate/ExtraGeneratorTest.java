package com.lottery.generate;

import com.lottery.domain.ExtraLottery;
import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Field;
import com.lottery.domain.output.Ticket;
import com.lottery.exception.WrongGeneratorConditionException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

public class ExtraGeneratorTest {

    public static final int FIELDS_NUMBER = 2;
    public static final int PIECE_OF_NUMBER = 2;
    public static final int KEY_0 = 0;
    public static final int KEY_1 = 1;

    @InjectMocks
    private ExtraGenerator underTest;
    @Mock
    private SimpleGenerator simpleGenerator;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGenerateRandomNumberShouldReturnRightPieceOfNumber() {
        //GIVEN
        ExtraLottery extraLottery = createExtraLottery();
        when(simpleGenerator.generate(extraLottery))
                .thenReturn(createTickets())
                .thenReturn(createTickets());

        //WHEN
        Ticket actual = underTest.generate(extraLottery);

        //THEN
        assertEquals(FIELDS_NUMBER, actual.getFields().size());
        var fields = actual.getFields();
        var firstField = fields.get(0);
        var secondField = fields.get(1);
        assertEquals(PIECE_OF_NUMBER, firstField.getNumbers().size());
        assertEquals(PIECE_OF_NUMBER, secondField.getNumbers().size());
    }

    @Test
    public void testGenerateRandomNumberShouldThrowWrongGeneratorConditionException() {
        //GIVEN
        ExtraLottery extraLottery = createExtraLottery();
        extraLottery.setFieldsNumber(0);
        when(simpleGenerator.generate(extraLottery)).thenReturn(createTickets());

        //WHEN

        //THEN
        assertThrows(WrongGeneratorConditionException.class, () -> {
            underTest.generate(extraLottery);
        });
    }

    @Test
    public void testGetStrategy() {
        //GIVEN
        //WHEN
        StrategyType actual = underTest.getStrategyType();

        //THEN
        assertEquals(StrategyType.EXTRA, actual);
    }

    private ExtraLottery createExtraLottery() {
        ExtraLottery extraLottery = new ExtraLottery();
        extraLottery.setFieldsNumber(FIELDS_NUMBER);
        extraLottery.setPieceOfNumber(PIECE_OF_NUMBER);
        return extraLottery;
    }

    private Ticket createTickets() {
        Field field = new Field(List.of(1, 2));
        return new Ticket(List.of(field));
    }
}
