package com.lottery.generate;

import com.lottery.domain.StrategyType;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FieldsGeneratorTest {

    @InjectMocks
    private FieldsGenerator underTest;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetStrategy() {
        //GIVEN
        //WHEN
        StrategyType actual = underTest.getStrategyType();

        //THEN
        assertEquals(StrategyType.FIELDS, actual);
    }
}
