package com.lottery.generate;


import org.mockito.InjectMocks;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertThrows;
import static org.testng.Assert.assertTrue;

public class NumberGeneratorTest {

    public static final int LESS_THAN_MIN = -1;
    public static final int MIN_NUMBER = 0;
    public static final int MAX_NUMBER = 10;

    @InjectMocks
    private NumberGenerator underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testGenerateRandomNumberShouldReturnNumberWhenBetweenTwoNumber() {
        //GIVEN in setUp

        //WHEN
        int actual = underTest.generateRandomNumber(MIN_NUMBER, MAX_NUMBER);

        //THEN
        assertTrue(actual >= MIN_NUMBER);
        assertTrue(actual <= MAX_NUMBER);
    }

    @Test
    public void testGenerateRandomNumberShouldThrowIllegalArgumentExceptionWhenMaxNumberLessThanMinNumber() {
        //GIVEN in setUp
        //WHEN

        //THEN
        assertThrows(IllegalArgumentException.class, () -> {
            underTest.generateRandomNumber(MIN_NUMBER, LESS_THAN_MIN);
        });
    }
}
