package com.lottery.generate;

import com.lottery.domain.StrategyType;
import com.lottery.exception.IncorrectStrategyException;
import org.mockito.Mock;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class GeneratorFactoryTest {

    public static final StrategyType SIMPLE = StrategyType.SIMPLE;

    @Mock
    private SimpleGenerator simpleGenerator;

    private Set<Generator> generators;
    private GeneratorFactory underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);

        generators = Set.of(simpleGenerator);
        when(simpleGenerator.getStrategyType()).thenReturn(SIMPLE);
        underTest = new GeneratorFactory(generators);
    }

    @Test
    public void testFindGenerator() {
        //GIVEN in setUp

        //WHEN
        Generator<SimpleGenerator> actual = underTest.findGenerator(SIMPLE);

        //THEN
        verify(simpleGenerator, times(1)).getStrategyType();
        Assert.isInstanceOf(SimpleGenerator.class, actual);
    }

    @Test
    public void testFindGeneratorShouldThrowExceptionWhenDoesNotFind() {
        //GIVEN in setUp
        //WHEN

        //THEN
        org.testng.Assert.assertThrows(IncorrectStrategyException.class, () -> {
            underTest.findGenerator(null);
        });
    }
}
