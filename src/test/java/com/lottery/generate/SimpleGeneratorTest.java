package com.lottery.generate;

import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Field;
import com.lottery.domain.output.Ticket;
import com.lottery.exception.WrongGeneratorConditionException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.lottery.domain.StrategyType.SIMPLE;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

public class SimpleGeneratorTest {

    public static final int MIN_NUMBER = 1;
    public static final int MAX_NUMBER = 2;
    public static final int PIECE_OF_NUMBER = 2;

    @InjectMocks
    private SimpleGenerator underTest;
    @Mock
    private NumberGenerator numberGenerator;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGenerateRandomNumberShouldReturnRightPieceOfNumber() {
        //GIVEN
        SimpleLottery simpleLottery = new SimpleLottery();
        simpleLottery.setMaxNumber(MAX_NUMBER);
        simpleLottery.setPieceOfNumber(PIECE_OF_NUMBER);
        when(numberGenerator.generateRandomNumber(MIN_NUMBER, MAX_NUMBER)).thenReturn(1).thenReturn(2);

        //WHEN
        Ticket actual = underTest.generate(simpleLottery);

        //THEN
        assertEquals(1, actual.getFields().size());
        Field field = actual.getFields().get(0);
        assertEquals(PIECE_OF_NUMBER, field.getNumbers().size());
    }

    @Test
    public void testGenerateRandomNumberShouldThrowWrongGeneratorConditionException() {
        //GIVEN
        SimpleLottery simpleLottery = new SimpleLottery();
        simpleLottery.setMaxNumber(MIN_NUMBER);
        simpleLottery.setPieceOfNumber(PIECE_OF_NUMBER);
        when(numberGenerator.generateRandomNumber(MIN_NUMBER, MIN_NUMBER)).thenReturn(1);

        //WHEN

        //THEN
        assertThrows(WrongGeneratorConditionException.class, () -> {
            underTest.generate(simpleLottery);
        });
    }

    @Test
    public void testGetStrategy() {
        //GIVEN
        //WHEN
        StrategyType actual = underTest.getStrategyType();

        //THEN
        assertEquals(SIMPLE, actual);
    }

    private List<Integer> maptToList(Map<Integer, List<Integer>> map) {
        return map.values().stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

}
