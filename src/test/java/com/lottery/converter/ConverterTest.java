package com.lottery.converter;

import com.lottery.domain.SimpleLottery;
import com.lottery.scan.domain.SimpleLotteryXML;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class ConverterTest {

    @InjectMocks
    private Converter<SimpleLotteryXML, SimpleLottery> underTest;
    @Mock
    private ModelMapper modelMapper;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testConvert() {
        //GIVEN
        SimpleLotteryXML source = createSimpleLotteryXML();
        SimpleLottery destination = createSimpleLottery();
        Mockito.when(modelMapper.map(source, SimpleLottery.class)).thenReturn(destination);

        //WHEN
        SimpleLottery actual = underTest.convert(source, SimpleLottery.class);

        //THEN
        assertEquals(destination, actual);
    }

    private SimpleLottery createSimpleLottery() {
        SimpleLottery simpleLottery = new SimpleLottery();
        simpleLottery.setMaxNumber(5);
        simpleLottery.setPieceOfNumber(1);
        return simpleLottery;
    }

    private SimpleLotteryXML createSimpleLotteryXML() {
        SimpleLotteryXML simpleLottery = new SimpleLotteryXML();
        simpleLottery.setMaxNumber(5);
        simpleLottery.setPieceOfNumber(1);
        return simpleLottery;
    }
}
