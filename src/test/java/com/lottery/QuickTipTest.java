package com.lottery;

import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.domain.output.Field;
import com.lottery.domain.output.Ticket;
import com.lottery.generate.Generator;
import com.lottery.generate.GeneratorFactory;
import com.lottery.print.Printout;
import com.lottery.scan.ScannerFactory;
import com.lottery.scan.service.Scanner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class QuickTipTest {

    public static final Ticket TICKET = new Ticket(List.of(new Field(List.of(1))));
    public static final int TICKET_NUMBER = 2;
    private static StrategyType SIMPLE = StrategyType.SIMPLE;

    @Mock
    private ScannerFactory scannerFactory;
    @Mock
    private GeneratorFactory generatorFactory;
    @Mock
    private Printout<List<Ticket>> printout;
    @Mock
    private Scanner scanner;
    @Mock
    private Generator generator;
    @InjectMocks
    private QuickTip underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testCreateTip() {
        //GIVEN
        SimpleLottery simpleLottery = createSimpleLottery();
        when(scannerFactory.findScanner(SIMPLE)).thenReturn(scanner);
        when(scanner.scan()).thenReturn(simpleLottery);
        when(generatorFactory.findGenerator(SIMPLE)).thenReturn(generator);
        when(generator.generate(simpleLottery)).thenReturn(TICKET);

        //WHEN
        underTest.createTip(SIMPLE, TICKET_NUMBER);

        //THEN
        verify(printout, times(1)).print(List.of(TICKET, TICKET));
        verify(scanner, times(1)).scan();
        verify(scannerFactory, times(1)).findScanner(SIMPLE);
        verify(generator, times(TICKET_NUMBER)).generate(simpleLottery);
        verify(generatorFactory, times(1)).findGenerator(SIMPLE);
    }

    private SimpleLottery createSimpleLottery() {
        SimpleLottery simpleLottery = new SimpleLottery();
        simpleLottery.setMaxNumber(5);
        simpleLottery.setPieceOfNumber(1);
        return simpleLottery;
    }
}
