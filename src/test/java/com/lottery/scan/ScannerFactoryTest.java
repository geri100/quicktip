package com.lottery.scan;

import com.lottery.domain.StrategyType;
import com.lottery.exception.IncorrectStrategyException;
import com.lottery.scan.service.Scanner;
import com.lottery.scan.service.SimpleXMLScanner;
import org.mockito.Mock;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ScannerFactoryTest {

    public static final StrategyType SIMPLE = StrategyType.SIMPLE;

    @Mock
    private SimpleXMLScanner simpleXMLScanner;

    private Set<Scanner> scanners;
    private ScannerFactory underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);

        scanners = Set.of(simpleXMLScanner);
        when(simpleXMLScanner.getStrategyType()).thenReturn(SIMPLE);
        underTest = new ScannerFactory(scanners);
    }

    @Test
    public void testFindGenerator() {
        //GIVEN in setUp

        //WHEN
        Scanner<SimpleXMLScanner> actual = underTest.findScanner(SIMPLE);

        //THEN
        verify(simpleXMLScanner, times(1)).getStrategyType();
        Assert.isInstanceOf(Scanner.class, actual);
    }

    @Test
    public void testFindGeneratorShouldThrowExceptionWhenDoesNotFind() {
        //GIVEN in setUp
        //WHEN

        //THEN
        org.testng.Assert.assertThrows(IncorrectStrategyException.class, () -> {
            underTest.findScanner(null);
        });
    }
}
