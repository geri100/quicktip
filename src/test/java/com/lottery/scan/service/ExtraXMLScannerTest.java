package com.lottery.scan.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.converter.Converter;
import com.lottery.domain.ExtraLottery;
import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.reader.FileReader;
import com.lottery.scan.domain.ExtraLotteryXML;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.lottery.domain.StrategyType.EXTRA;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class ExtraXMLScannerTest {

    private static final String FILE_CONTENT = "fileContent";

    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private Converter<ExtraLotteryXML, ExtraLottery> converter;
    @Mock
    private FileReader fileReader;

    private ExtraXMLScanner underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);

        underTest = new ExtraXMLScanner(objectMapper, converter, fileReader);
    }

    @Test
    public void testScan() throws IOException {
        //GIVEN
        ExtraLotteryXML extraLotteryXML = createExtraLotteryXML();
        ExtraLottery extraLottery = createExtraLottery();
        when(fileReader.read(any())).thenReturn(FILE_CONTENT);
        when(objectMapper.readValue(FILE_CONTENT, ExtraLotteryXML.class)).thenReturn(extraLotteryXML);
        when(converter.convert(extraLotteryXML, ExtraLottery.class)).thenReturn(extraLottery);

        //WHEN
        SimpleLottery actual = underTest.scan();

        //THEN
        ExtraLottery expected = createExtraLottery();
        assertEquals(actual, expected);

    }

    @Test
    public void testGetStrategy() {
        //GIVEN
        //WHEN
        StrategyType actual = underTest.getStrategyType();

        //THEN
        assertEquals(EXTRA, actual);
    }

    private ExtraLottery createExtraLottery() {
        ExtraLottery extraLottery = new ExtraLottery();
        extraLottery.setFieldsNumber(2);
        extraLottery.setPieceOfNumber(1);
        return extraLottery;
    }

    private ExtraLotteryXML createExtraLotteryXML() {
        ExtraLotteryXML extraLottery = new ExtraLotteryXML();
        extraLottery.setFieldsNumber(2);
        extraLottery.setPieceOfNumber(1);
        return extraLottery;
    }
}
