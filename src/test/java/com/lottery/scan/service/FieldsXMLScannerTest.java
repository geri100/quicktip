package com.lottery.scan.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.converter.Converter;
import com.lottery.domain.ExtraLottery;
import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.reader.FileReader;
import com.lottery.scan.domain.ExtraLotteryXML;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.lottery.domain.StrategyType.FIELDS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class FieldsXMLScannerTest {

    private static final String FILE_CONTENT = "fileContent";
    public static final int DEFAULT_MAX_NUMBER = 90;

    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private Converter<ExtraLotteryXML, ExtraLottery> converter;
    @Mock
    private FileReader fileReader;

    private FieldsXMLScanner underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);

        underTest = new FieldsXMLScanner(objectMapper, converter, fileReader, DEFAULT_MAX_NUMBER);
    }

    @Test
    public void testScan() throws IOException {
        //GIVEN
        ExtraLotteryXML fieldsLotteryXML = createFieldsLotteryXML();
        ExtraLottery extraLottery = createFieldsLottery();
        when(fileReader.read(any())).thenReturn(FILE_CONTENT);
        when(objectMapper.readValue(FILE_CONTENT, ExtraLotteryXML.class)).thenReturn(fieldsLotteryXML);
        when(converter.convert(fieldsLotteryXML, ExtraLottery.class)).thenReturn(extraLottery);

        //WHEN
        SimpleLottery actual = underTest.scan();

        //THEN
        ExtraLottery expected = createFieldsLottery();
        expected.setMaxNumber(DEFAULT_MAX_NUMBER);
        assertEquals(actual, expected);

    }

    @Test
    public void testGetStrategy() {
        //GIVEN
        //WHEN
        StrategyType actual = underTest.getStrategyType();

        //THEN
        assertEquals(FIELDS, actual);
    }

    private ExtraLottery createFieldsLottery() {
        ExtraLottery extraLottery = new ExtraLottery();
        extraLottery.setFieldsNumber(2);
        extraLottery.setPieceOfNumber(1);
        return extraLottery;
    }

    private ExtraLotteryXML createFieldsLotteryXML() {
        ExtraLotteryXML extraLotteryXML = new ExtraLotteryXML();
        extraLotteryXML.setFieldsNumber(2);
        extraLotteryXML.setPieceOfNumber(1);
        return extraLotteryXML;
    }
}
