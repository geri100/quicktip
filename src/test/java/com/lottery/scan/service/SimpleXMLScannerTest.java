package com.lottery.scan.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.converter.Converter;
import com.lottery.domain.SimpleLottery;
import com.lottery.domain.StrategyType;
import com.lottery.reader.FileReader;
import com.lottery.scan.domain.SimpleLotteryXML;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.lottery.domain.StrategyType.SIMPLE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class SimpleXMLScannerTest {

    private static final String FILE_CONTENT = "fileContent";

    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private Converter<SimpleLotteryXML, SimpleLottery> converter;
    @Mock
    private FileReader fileReader;

    private SimpleXMLScanner underTest;

    @BeforeMethod
    public void setUp() {
        initMocks(this);

        underTest = new SimpleXMLScanner(objectMapper, converter, fileReader);
    }

    @Test
    public void testScan() throws IOException {
        //GIVEN
        SimpleLotteryXML simpleLotteryXML = createSimpleLotteryXML();
        SimpleLottery simpleLottery = createSimpleLottery();
        when(fileReader.read(any())).thenReturn(FILE_CONTENT);
        when(objectMapper.readValue(FILE_CONTENT, SimpleLotteryXML.class)).thenReturn(simpleLotteryXML);
        when(converter.convert(simpleLotteryXML, SimpleLottery.class)).thenReturn(simpleLottery);

        //WHEN
        SimpleLottery actual = underTest.scan();

        //THEN
        SimpleLottery expected = createSimpleLottery();
        assertEquals(actual, expected);

    }

    @Test
    public void testGetStrategy() {
        //GIVEN
        //WHEN
        StrategyType actual = underTest.getStrategyType();

        //THEN
        assertEquals(SIMPLE, actual);
    }

    private SimpleLottery createSimpleLottery() {
        SimpleLottery simpleLottery = new SimpleLottery();
        simpleLottery.setMaxNumber(5);
        simpleLottery.setPieceOfNumber(1);
        return simpleLottery;
    }

    private SimpleLotteryXML createSimpleLotteryXML() {
        SimpleLotteryXML simpleLottery = new SimpleLotteryXML();
        simpleLottery.setMaxNumber(5);
        simpleLottery.setPieceOfNumber(1);
        return simpleLottery;
    }
}
