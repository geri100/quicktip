package com.lottery;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.env.Environment;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.lottery.domain.StrategyType.SIMPLE;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ApplicationRunnerTest {

    public static final String SIMPLE_STRATEGY = "SIMPLE";
    public static final String WRONG_STRATEGY = "wrong_strategy";
    public static final String TICKET_NUMBER = "1";
    @InjectMocks
    private ApplicationRunner underTest;
    @Mock
    private QuickTip quickTip;
    @Mock
    private Environment environment;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testRunShouldCallQuickTipOnce() {
        //GIVEN
        when(environment.getProperty(anyString())).thenReturn(SIMPLE_STRATEGY);

        //WHEN
        underTest.run(SIMPLE_STRATEGY, TICKET_NUMBER);

        //THEN
        verify(quickTip, times(1)).createTip(SIMPLE, Integer.parseInt(TICKET_NUMBER));
    }

    @Test
    public void testRunShouldThrowExceptionWhenArgsIsEmpty() {
        //GIVEN
        //WHEN

        //THEN
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            underTest.run();
        });
    }

    @Test
    public void testRunShouldThrowExceptionWhenArgsContainMoreThanTwoNumber() {
        //GIVEN
        //WHEN

        //THEN
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            underTest.run("1", "2", "3");
        });
    }

    @Test
    public void testRunShouldThrowExceptionWhenArgsContainOneNumber() {
        //GIVEN
        //WHEN

        //THEN
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            underTest.run("1");
        });
    }

    @Test
    public void testRunShouldThrowExceptionWhenStrategyNumberIsIncorrect() {
        //GIVEN
        when(environment.getProperty(anyString())).thenReturn(WRONG_STRATEGY);

        //WHEN

        //THEN
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            underTest.run(WRONG_STRATEGY);
        });
    }
}
